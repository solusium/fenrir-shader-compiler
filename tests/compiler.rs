use {
    fenrir_shader_compiler::{Compiler, Language, Metadata, Stage},
    pretty_assertions::assert_eq,
    std::{env::var, fs::read_dir},
    tracing::{error, level_filters::LevelFilter},
    tracing_subscriber::fmt::format::FmtSpan,
};

#[inline]
fn init_tracing() {
    assert!(tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_max_level(LevelFilter::TRACE)
        .try_init()
        .is_ok());
}

#[inline]
fn load_compiler(name: &str) -> Compiler {
    let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

    assert!(mainifest_dir_result.is_ok());

    let mut library_dir = mainifest_dir_result.unwrap();

    library_dir.push_str("/target/");

    library_dir.push_str(mode());

    library_dir.push_str("/deps");

    let maybe_entires = read_dir(&library_dir);

    assert!(maybe_entires.is_ok());

    let maybe_compiler = maybe_entires
        .unwrap()
        .filter_map(|entry_result| entry_result.ok())
        .filter(|entry| entry.file_name().to_string_lossy().contains(name))
        .filter_map(|entry| match Compiler::new(&entry.path()) {
            Err(error) => {
                error!("{entry:?}: {error}");
                None
            }
            Ok(module) => Some(module),
        })
        .find(|_| true);

    assert!(maybe_compiler.is_some());

    maybe_compiler.unwrap()
}

#[inline]
fn load_compilers() -> impl IntoIterator<Item = Compiler> {
    ["naga"].into_iter().map(load_compiler)
}

#[inline]
#[cfg(debug_assertions)]
const fn mode() -> &'static str {
    "debug"
}

#[inline]
#[cfg(not(debug_assertions))]
const fn mode() -> &'static str {
    "release"
}

#[test]
fn read_and_compile_shader() {
    init_tracing();

    let glsl = "#version 450\n\
               \n\
               vec2 positions[3] = vec2[](\n\
                 vec2(0.0, -0.5),\n\
                 vec2(0.5, 0.5),\n\
                 vec2(-0.5, 0.5)\n\
               );\n\
               \n\
               void main() {\n\
                 gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
               }\n";

    let metadata = Metadata::build(
        glsl.as_bytes(),
        Language::GLSL,
        Language::SPIRV,
        Stage::Vertex,
    )
    .build();

    for compiler in load_compilers() {
        {
            let glsl_module_result =
                compiler.read(glsl.as_bytes(), Language::GLSL, Some(Stage::Vertex));

            assert!(glsl_module_result.is_ok());

            let glsl_module = glsl_module_result.unwrap();

            let entry_points_result = glsl_module.entry_points();

            assert!(entry_points_result.is_ok());

            let entry_points = entry_points_result.unwrap();

            assert_eq!(entry_points.len(), 1);
            assert_eq!(entry_points[0].name(), "main");
            assert_eq!(entry_points[0].stage(), Stage::Vertex);
        }

        let spirv_result = compiler.compile(metadata);

        assert!(spirv_result.is_ok());

        let spirv = spirv_result.unwrap();

        let spirv_module_result = compiler.read(&spirv, Language::SPIRV, None);

        assert!(spirv_module_result.is_ok());

        let spirv_module = spirv_module_result.unwrap();

        let entry_points_result = spirv_module.entry_points();

        assert!(entry_points_result.is_ok());

        let entry_points = entry_points_result.unwrap();

        assert_eq!(entry_points.len(), 1);
        assert_eq!(entry_points[0].name(), "main");
        assert_eq!(entry_points[0].stage(), Stage::Vertex);
    }
}
