use {
    crate::{Language, Stage},
    abi_stable::{std_types::RSlice, StableAbi},
};

pub struct Builder<'a> {
    source: &'a [u8],
    source_language: Language,
    target_language: Language,
    stage: Stage,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Metadata<'a> {
    source: RSlice<'a, u8>,
    source_language: Language,
    target_language: Language,
    stage: Stage,
}

impl<'a> Builder<'a> {
    #[inline]
    #[must_use]
    pub fn build(self) -> Metadata<'a> {
        let source = self.source.into();
        let source_language = self.source_language;
        let target_language = self.target_language;
        let stage = self.stage;

        Metadata {
            source,
            source_language,
            target_language,
            stage,
        }
    }
}

impl<'a> Metadata<'a> {
    #[inline]
    #[must_use]
    pub const fn build(
        source: &'a [u8],
        source_language: Language,
        target_language: Language,
        stage: Stage,
    ) -> Builder<'a> {
        Builder {
            source,
            source_language,
            target_language,
            stage,
        }
    }

    /// Get the source code.
    #[inline]
    #[must_use]
    pub fn source(&self) -> &'a [u8] {
        self.source.into()
    }

    /// The language of the source code.
    #[inline]
    #[must_use]
    pub const fn source_language(&self) -> Language {
        self.source_language
    }

    /// The shader stage of the source code.
    #[inline]
    #[must_use]
    pub const fn stage(&self) -> Stage {
        self.stage
    }

    /// The language we want the source code to compile to.
    #[inline]
    #[must_use]
    pub const fn target_language(&self) -> Language {
        self.target_language
    }
}
