use {
    crate::{shader::stable_abi::Trait_TO, EntryPoint},
    abi_stable::{
        std_types::{
            RBox,
            RResult::{RErr, ROk},
        },
        StableAbi,
    },
    fenrir_error::Error,
};

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding
    )]

    use {
        crate::EntryPoint,
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RVec},
        },
        fenrir_error::Error,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Debug + Eq + PartialEq + Send + Sync + 'static {
        fn entry_points(&self) -> RResult<RVec<EntryPoint<'_>>, Error>;
    }
}

// Representation of a shader module.
#[repr(C)]
#[derive(Debug, Eq, PartialEq, StableAbi)]
pub struct Module {
    inner: Trait_TO<RBox<()>>,
}

impl Module {
    // Construct a new shader module.
    #[inline]
    #[must_use]
    pub const fn new(inner: Trait_TO<RBox<()>>) -> Self {
        Self { inner }
    }

    /// Entry points of this shader module.
    ///
    /// # Errors
    ///
    /// This function might return an error if casting an entry point from the internal
    /// representation to [`EntryPoint`] failed.
    #[inline]
    pub fn entry_points(&self) -> Result<Vec<EntryPoint<'_>>, Error> {
        match self.inner.entry_points() {
            ROk(entry_points) => Ok(entry_points.into()),
            RErr(error) => Err(error),
        }
    }
}
