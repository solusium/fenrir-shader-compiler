#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(clippy::expl_impl_clone_on_copy, improper_ctypes_definitions)]

use {
    crate::stable_abi::ModuleRef,
    abi_stable::{
        library::RootModule,
        std_types::{
            RArc,
            RResult::{RErr, ROk},
        },
        DynTrait, StableAbi,
    },
    educe::Educe,
    fenrir_error::{initialization_failed, Error as FenrirError},
    std::path::Path,
    tracing::error,
};
pub use {
    crate::{entry_point::EntryPoint, error::CompileError, metadata::Metadata},
    error::Error,
};

mod entry_point;
mod error;
mod metadata;
pub mod shader;

/// Declarations necessary for a stable abi.
pub mod stable_abi {
    #![allow(clippy::expl_impl_clone_on_copy, clippy::must_use_candidate)]

    use {
        crate::{Error, Inner, Language, Metadata, Stage},
        abi_stable::{
            declare_root_module_statics,
            library::RootModule,
            package_version_strings,
            sabi_types::VersionStrings,
            std_types::{RArc, ROption, RResult, RSlice, RVec},
            DynTrait, StableAbi,
        },
    };

    /// The root module of a dynamic library, allowing to load it as a shader compiler.
    #[repr(C)]
    #[derive(StableAbi)]
    #[sabi(kind(Prefix(prefix_fields = ModulePrefix, prefix_ref = ModuleRef)))]
    pub struct Module {
        pub new: extern "C" fn() -> DynTrait<'static, RArc<()>, Inner>,

        pub compile: extern "C" fn(
            &DynTrait<'static, RArc<()>, Inner>,
            Metadata,
        ) -> RResult<RVec<u8>, Error>,

        #[sabi(last_prefix_field)]
        pub read: extern "C" fn(
            &DynTrait<'static, RArc<()>, Inner>,
            RSlice<'_, u8>,
            Language,
            ROption<Stage>,
        ) -> RResult<crate::shader::Module, Error>,
    }

    #[allow(clippy::use_self)]
    impl RootModule for ModuleRef {
        declare_root_module_statics! {ModuleRef}

        const BASE_NAME: &'static str = "fenrir_shader_compiler";

        const NAME: &'static str = "fenrir-shader-compiler";

        const VERSION_STRINGS: VersionStrings = package_version_strings!();
    }
}

/// Supported shader languages for compilation.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Language {
    GLSL,
    SPIRV,
}

/// Supported shader stages for compilation.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Stage {
    Vertex,
    Compute,
    Fragment,
}

/// Interface struct for the stable abi interface.
#[repr(C)]
#[derive(StableAbi)]
#[sabi(impl_InterfaceType(Clone, Debug, Send))]
pub struct Inner;

/// Compiles shader source code from on language into another.
#[repr(C)]
#[derive(Educe)]
#[educe(Debug)]
#[derive(Clone, StableAbi)]
pub struct Compiler {
    #[educe(Debug(ignore))]
    module: ModuleRef,
    inner: DynTrait<'static, RArc<()>, Inner>,
}

impl Compiler {
    /// Try to load a shader compiler from path.
    ///
    /// # Errors
    ///
    /// This function will return
    /// [`InitializationFailed`](fenrir_error::Error::InitializationFailed) if we couldn't load a
    /// shader compiler from the given path.
    #[inline]
    pub fn new(path: &Path) -> Result<Self, FenrirError> {
        match ModuleRef::load_from_file(path) {
            Ok(module) => {
                let inner = module.new()();

                Ok(Self { module, inner })
            }
            Err(error) => {
                let message = format!("failed to load a shader compiler from {path:?}: {error:?}");

                error!(message);

                Err(initialization_failed(&message))
            }
        }
    }

    /// Try to compile [`metadata.source()`](crate::Metadata::source) into
    /// [`metadata.target_language()`](crate::Metadata::target_language) code.
    ///
    /// # Errors
    ///
    /// This function will return
    /// [`InvalidArgument`](fenrir_error::Error::InvalidArgument) if we couldn't read
    /// `metadata.source()` as code of
    /// [`metadata.source_language()`](crate::Metadata::source_language).
    /// If `metadata.source()` contains errors (e.g. syntax errors), we will return a vector of
    /// [`CompileErrors`](crate::CompileError).
    #[inline]
    pub fn compile(&self, metadata: Metadata) -> Result<Vec<u8>, Error> {
        match self.module.compile()(&self.inner, metadata) {
            ROk(bytes) => Ok(bytes.into()),
            RErr(error) => Err(error),
        }
    }

    /// Try to read a shader mode from source code.
    ///
    /// # Errors
    ///
    /// This function will return
    /// [`InvalidArgument`](fenrir_error::Error::InvalidArgument) if we couldn't read 'source' as
    /// code of 'language'.
    #[inline]
    pub fn read(
        &self,
        source: &[u8],
        language: Language,
        stage: Option<Stage>,
    ) -> Result<crate::shader::Module, Error> {
        self.module.read()(&self.inner, source.into(), language, stage.into()).into()
    }
}

unsafe impl Send for Compiler {}
unsafe impl Sync for Compiler {}
