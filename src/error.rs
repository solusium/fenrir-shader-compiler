use {
    abi_stable::{
        rtuple,
        std_types::{
            ROption::{self, RNone, RSome},
            RString, RVec, Tuple2,
        },
        StableAbi,
    },
    core::ops::Range,
    fenrir_error::Error as FenrirError,
    thiserror_no_std::Error,
};

#[repr(C)]
#[derive(Clone, Debug, Error, Eq, PartialEq, StableAbi)]
pub enum Error {
    #[error("{}", 0)]
    CompileError(RVec<CompileError>),
    #[error("{0}")]
    FenrirError(FenrirError),
}

#[repr(C)]
#[derive(Error)]
#[error("{reason}")]
#[allow(clippy::module_name_repetitions)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct CompileError {
    range: ROption<Tuple2<usize, usize>>,
    reason: RString,
}

impl CompileError {
    #[inline]
    #[must_use]
    pub fn new(range: Option<Range<usize>>, reason: &str) -> Self {
        let range = range.map_or(RNone, |range_| RSome(rtuple!(range_.start, range_.end)));

        let reason = RString::from(reason);

        Self { range, reason }
    }

    #[inline]
    #[must_use]
    pub const fn range(&self) -> Option<Range<usize>> {
        match self.range {
            RNone => None,
            RSome(range) => Some(Range {
                start: range.0,
                end: range.1,
            }),
        }
    }

    #[inline]
    #[must_use]
    pub fn reason(&self) -> &str {
        &self.reason
    }
}
