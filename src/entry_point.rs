use {
    crate::Stage,
    abi_stable::{std_types::RStr, StableAbi},
};

/// The main function of a pipeline [´Stage´](crate::Stage).
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct EntryPoint<'a> {
    name: RStr<'a>,
    stage: Stage,
    workgroup_size: [usize; 3],
}

impl<'a> EntryPoint<'a> {
    // Construct a new entry point.
    #[inline]
    #[must_use]
    pub fn new(name: &'a str, stage: Stage, workgroup_size: [usize; 3]) -> Self {
        let name = name.into();

        Self {
            name,
            stage,
            workgroup_size,
        }
    }

    // Name of the entry point.
    #[inline]
    #[must_use]
    pub fn name(&self) -> &str {
        &self.name
    }

    // Shader stage of the entry point.
    #[inline]
    #[must_use]
    pub const fn stage(&self) -> Stage {
        self.stage
    }

    // Workgroup size for compute shaders.
    #[inline]
    #[must_use]
    pub const fn workgroup_size(&self) -> [usize; 3] {
        self.workgroup_size
    }
}
